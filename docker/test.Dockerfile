FROM node:12.14.0-alpine

ENV NODE_ENV=test

COPY files/docker-entrypoint.sh /usr/local/bin/
RUN ln -s /usr/local/bin/docker-entrypoint.sh /
ENTRYPOINT [ "docker-entrypoint.sh" ]

RUN apk add --no-cache \
  git bash

# HACK: use bash to avoid issues when running yarn commands ??
# https://github.com/yarnpkg/yarn/issues/6686#issuecomment-571419741
SHELL [ "/bin/bash", "-c"]
RUN yarn config set script-shell bash
RUN npm config set script-shell bash

WORKDIR /app
