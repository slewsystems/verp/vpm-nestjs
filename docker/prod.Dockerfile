FROM node:12.14.0-alpine

RUN apk add --no-cache \
  git bash

# HACK: use bash to avoid issues when running yarn commands ??
# https://github.com/yarnpkg/yarn/issues/6686#issuecomment-571419741
SHELL [ "/bin/bash", "-c"]

WORKDIR /app
COPY ./ ./
RUN yarn install && yarn build

ENV NODE_ENV=production
EXPOSE 80 443
CMD [ "node", "dist/main" ]
