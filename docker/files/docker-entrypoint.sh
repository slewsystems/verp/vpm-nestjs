#!/bin/bash
set -e

echo "                                                    "
echo " ____      ____      _____        ______  _______   "
echo "|    |    |    | ___|\    \      |      \/       \  "
echo "|    |    |    ||    |\    \    /          /\     \ "
echo "|    |    |    ||    | |    |  /     /\   / /\     |"
echo "|    |    |    ||    |/____/| /     /\ \_/ / /    /|"
echo "|     \  /     ||    ||    |||     |  \|_|/ /    / |"
echo "|\     \/     /||    ||____|/|     |       |    |  |"
echo "| \ ________ / ||____|       |\____\       |____|  /"
echo " \ |        | / |    |       | |    |      |    | / "
echo "  \|________|/  |____|        \|____|      |____|/  "
echo "    \(    )/      \(             \(          )/     "
echo "     '    '        '              '          '      "
echo "   By Slew Systems                                  "
echo "                                                    "

# install dependencies at workspace root
yarn install --silent --non-interactive

echo "WARNING: Working out of workspace root at '$PWD'!"

exec "$@"
