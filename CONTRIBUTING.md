# Development

1. Setup the app for the first time (or to start from scratch)

    If you do not have DIP installed you can find [installation instuctions here](https://github.com/bibendi/dip#installation).
    Installing through Homebrew is preferred if you use macOS.

    ```bash
    # spin up the nginx proxy and dns server
    # you should only need to start them once
    dip nginx up --domain=slewsystems
    dip dns up --domain=slewsystems

    # you will only need to run this once, ever
    echo "127.0.0.1 vpm.slewsystems" | sudo tee -a /etc/hosts

    dip provision
    ```

1. Start a service

    ```bash
    DIP_PKG=@slewsystems/vpm-PACKAGE-NAME dip start
    ```

1. Access it

    ```bash
    open http://vpm.slewsystems
    ```

# Code Standards

Below is a breakdown of the tools we use and what their purpose is:

-   **[Editorconfig](https://editorconfig.org/)**: Configures your text editor or IDE to use the "correct" indentation and line endings.
-   **[Prettier](https://prettier.io/)**: Automatically fixes code formatting in JS/JSX and Markdown files.
-   **[TSLint](https://palantir.github.io/tslint/)**: Lints TS/TSX files to ensure good code quality.
-   **[TypeScript](https://www.typescriptlang.org/)**: Ensures JavaScript typings are correct and them some.

# Testing

-   Unit Tests: `DIP_PKG=@slewsystems/vpm-PACKAGE-NAME dip run test`
-   End-to-end Tests: `DIP_PKG=@slewsystems/vpm-PACKAGE-NAMEe dip run test e2e`
-   Linting: `DIP_PKG=@slewsystems/vpm-PACKAGE-NAME dip run lint`

# Debugging

## Debugging the backend

// TODO

## Debugging the frontend

// TODO

## Debugging CI jobs

We use Gitlab's CI service to run automated testing and deployments. When making changes to CI configuration (in the `.gitlab-ci.yml` file) rather than making commits and testing in the cloud CI servers we can instead, run a job locally via [`gitlab-runner exec`](https://docs.gitlab.com/runner/commands/#gitlab-runner-exec). Doing so allows you to interate much more quickly!

```bash
# just need to install it once
brew install gitlab-runner

# next you'll need to make a minor modification to the gitlab-ci.yml:
# this is a bit of a hack, but due to differences in the cloud environemnt
# versus the local one as well as limitations of the 'exec' command, we'll need to comment out the entire 'variables' section.

# since running most jobs requires pulling an image from the registry and we want to run our job locally, we'll need to authenticate. To do so, you'll need to generate a personal access token in gitlab with at at least read_registry permissions. Set the REGISTRY_USER and REGISTRY_TOKEN values.

# now we can run a job, in this case, the 'lint' job
gitlab-runner exec docker
    --docker-privileged
    --env REGISTRY_USER=YOUR_USERNAME
    --env REGISTRY_TOKEN=YOUR_PERSONAL_TOKEN
    --env CI_REGISTRY=registry.gitlab.com
    --env DOCKER_HOST=tcp://docker:2375
    --env DOCKER_TLS_CERTDIR=''
    --env CONTAINER_TEST_IMAGE=registry.gitlab.com/slewsystems/verp/vpm-nestjs:test-master
    lint
```
