# VPM: Video Production Manager (Working Title)

**⚠️ This project is actively being developed and is not ready for production use.**

_Web application and GraphQL API for VPM built with [TypeScript](https://www.typescriptlang.org/) using [NestJS](https://github.com/nestjs/nest)._

# Features (in development)

-   [ ] Crewing (hiring, ranking, tracking)
-   [ ] Preproduction
-   [ ] Postproduction
-   [ ] Vechicle and equipment inventorying

## Contributing

Please see documentation for [contributing here](./CONTRIBUTING.md).
